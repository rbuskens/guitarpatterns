//
//  Guitar_PatternsTests.m
//  Guitar PatternsTests
//
//  Created by Rob Buskens on 2013-07-08.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Guitar_PatternsTests : XCTestCase

@end

@implementation Guitar_PatternsTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
