//
//  main.m
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-08.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BAGPAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([BAGPAppDelegate class]));
	}
}
