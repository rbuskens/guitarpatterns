//
//  BAGPFretboardView.h
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-10.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BAGPFretboardView : UIView
@property float zoomScale; // TODO, maintain zoom scale here as scroll view will fudgeit.... (transform... or the like)

- (void) centerOnFret: (int) fretNumber;

@end
