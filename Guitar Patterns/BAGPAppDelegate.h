//
//  BAGPAppDelegate.h
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-08.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BAGPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
