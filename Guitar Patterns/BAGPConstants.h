//
//  BAGPConstants.h
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-31.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

extern int const kBAGPFretSpacing;
extern int const kBAGPStringSpacing;

@interface BAGPConstants : NSObject

@end
