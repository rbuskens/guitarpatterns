//
//  BAGPViewController.m
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-08.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "BAGPViewController.h"
#import "BAGPPatternFactory.h"
#import "BAGPDisplayTone.h"
#import "BAGPAudioManager.h"

@interface BAGPViewController ()
@property (strong, nonatomic) UIActionSheet *keyActionSheet;
@property (strong, nonatomic) UIActionSheet *scaleActionSheet;
@property (strong, nonatomic) UIActionSheet *patternTypeActionsheet;
@property (strong, nonatomic) UIActionSheet *patternActionSheet;
@property (strong, nonatomic) NSDictionary *tmpDict;
@property (strong, nonatomic) NSArray *fretboardTones;
@property (strong, nonatomic) NSDictionary *patternPositions;
@property (strong, nonatomic) BAGPAudioManager *audioManager;

@property int currentPattern;
@property NSString *currentKey;
@end

@implementation BAGPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	_currentKey = @"C";
	_currentPattern = 1;
	
	_tmpDict = @{
				 @1:@"C",
				 @1.5: @"C#/Db",
				 @2:@"D",
				 @2.5: @"D#/Eb",
				 @3:@"E",
				 @4:@"F",
				 @4.5: @"F#/Gb",
				 @5:@"G",
				 @5.5: @"G#/Ab",
				 @6:@"A",
				 @6.5: @"A#/Bb",
				 @7:@"B"
				 };
	
	// setting string 0 to be empty... so that one can use strings 1..6 as direct indexes. probably bad. ???
	_fretboardTones = @[
						@[],
						@[	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@6.5,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4	],
						@[	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@7,	@1,	@1.5	],
						@[	@5,	@5.5,	@6,	@6.5,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5	],
						@[	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@6.5,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@7,	@1,	@1.5,	@2,	@2.5	],
						@[	@6,	@6.5,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@6.5,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6	],
						@[	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@6.5,	@7,	@1,	@1.5,	@2,	@2.5,	@3,	@4,	@4.5,	@5,	@5.5,	@6,	@6.5,	@7,	@1,	@1.5,	@2,	@2.5,	@3	]
						];

	_patternPositions = @{
						 @"C,Major,1":		@0,
						 @"C,Major,2":		@1,
						 @"C,Major,3":		@4,
						 @"C,Major,4":		@7,
						 @"C,Major,5":		@9,
						 @"C#,Major,1":		@1,
						 @"C#,Major,2":		@2,
						 @"C#,Major,3":		@5,
						 @"C#,Major,4":		@8,
						 @"C#,Major,5":		@10,
						 @"Db,Major,1":		@1,
						 @"Db,Major,2":		@2,
						 @"Db,Major,3":		@5,
						 @"Db,Major,4":		@8,
						 @"Db,Major,5":		@10,
						 @"D,Major,1":		@2,
						 @"D,Major,2":		@3,
						 @"D,Major,3":		@6,
						 @"D,Major,4":		@9,
						 @"D,Major,5":		@11,
						 @"D#,Major,1":		@3,
						 @"D#,Major,2":		@4,
						 @"D#,Major,3":		@7,
						 @"D#,Major,4":		@10,
						 @"D#,Major,5":		@12,
						 @"Eb,Major,1":		@3,
						 @"Eb,Major,2":		@4,
						 @"Eb,Major,3":		@7,
						 @"Eb,Major,4":		@10,
						 @"Eb,Major,5":		@12,
						 @"E,Major,1":		@4,
						 @"E,Major,2":		@5,
						 @"E,Major,3":		@8,
						 @"E,Major,4":		@11,
						 @"E,Major,5":		@13,
						 @"F,Major,1":		@5,
						 @"F,Major,2":		@6,
						 @"F,Major,3":		@9,
						 @"F,Major,4":		@12,
						 @"F,Major,5":		@2,
						 @"F#,Major,1":		@6,
						 @"F#,Major,2":		@7,
						 @"F#,Major,3":		@10,
						 @"F#,Major,4":		@1,
						 @"F#,Major,5":		@3,
						 @"Gb,Major,1":		@6,
						 @"Gb,Major,2":		@7,
						 @"Gb,Major,3":		@10,
						 @"Gb,Major,4":		@1,
						 @"Gb,Major,5":		@3,
						 @"G,Major,1":		@7,
						 @"G,Major,2":		@8,
						 @"G,Major,3":		@11,
						 @"G,Major,4":		@2,
						 @"G,Major,5":		@4,
						 @"G#,Major,1":		@8,
						 @"G#,Major,2":		@9,
						 @"G#,Major,3":		@12,
						 @"G#,Major,4":		@3,
						 @"G#,Major,5":		@5,
						 @"Ab,Major,1":		@8,
						 @"Ab,Major,2":		@9,
						 @"Ab,Major,3":		@12,
						 @"Ab,Major,4":		@3,
						 @"Ab,Major,5":		@5,
						 @"A,Major,1":		@9,
						 @"A,Major,2":		@10,
						 @"A,Major,3":		@1,
						 @"A,Major,4":		@4,
						 @"A,Major,5":		@6,
						 @"A#,Major,1":		@10,
						 @"A#,Major,2":		@11,
						 @"A#,Major,3":		@2,
						 @"A#,Major,4":		@5,
						 @"A#,Major,5":		@7,
						 @"Bb,Major,1":		@10,
						 @"Bb,Major,2":		@11,
						 @"Bb,Major,3":		@2,
						 @"Bb,Major,4":		@5,
						 @"Bb,Major,5":		@7,
						 @"B,Major,1":		@11,
						 @"B,Major,2":		@12,
						 @"B,Major,3":		@3,
						 @"B,Major,4":		@6,
						 @"B,Major,5":		@8
						 };
	
    // Set up the container view to hold your custom view hierarchy
    CGSize containerSize = CGSizeMake(2000.0f, 276.0f);
    self.fretboardView = [[BAGPFretboardView alloc] initWithFrame:(CGRect){.origin=CGPointMake(0.0f, 0.0f), .size=containerSize}];
	self.fretboardView.backgroundColor = [UIColor whiteColor];
	
    [self.fretboardScrollView addSubview:self.fretboardView];
	
	[self updateDisplayTones]; // show the tones based on the default
	
    // Tell the scroll view the size of the contents, and zoom factors
    self.fretboardScrollView.contentSize = containerSize;
	self.fretboardScrollView.maximumZoomScale = 10.0;
	self.fretboardScrollView.minimumZoomScale = 0.1;

	// Double tap to unzoom in/out
    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    twoFingerTapRecognizer.numberOfTapsRequired = 2;
    twoFingerTapRecognizer.numberOfTouchesRequired = 1;
    [self.fretboardScrollView addGestureRecognizer:twoFingerTapRecognizer];
	
	// setup popover controllers
	_keyActionSheet = [[UIActionSheet alloc] initWithTitle: @"Key"
												  delegate: self
										 cancelButtonTitle: @"Cancel"
									destructiveButtonTitle: nil
										 otherButtonTitles:@"C", @"C#", @"Db", @"D", @"D#",
														   @"Eb", @"E", @"F", @"F#", @"Gb",
															@"G", @"G#", @"Ab", @"A", @"A#", @"Bb", @"B", @"Cancel", nil];

	_scaleActionSheet = [[UIActionSheet alloc] initWithTitle: @"Scale"
												  delegate: self
										 cancelButtonTitle: @"Cancel"
									destructiveButtonTitle: nil
										 otherButtonTitles:@"Major", @"Minor", @"Ionian", @"Dorian", @"Phrygian", @"Lydian",@"Mixolydian",@"Aeolian",@"Locrian",@"Cancel", nil];

	_patternTypeActionsheet = [[UIActionSheet alloc] initWithTitle: @"Pattern Type"
													  delegate: self
											 cancelButtonTitle: @"Cancel"
										destructiveButtonTitle: nil
											 otherButtonTitles:@"Scale",@"Mode",@"Triad Arpeggio",@"Seventh Arpeggio",@"Cancel", nil];
	
	_patternActionSheet = [[UIActionSheet alloc] initWithTitle: @"Pattern"
													delegate: self
										   cancelButtonTitle: @"Cancel"
									  destructiveButtonTitle: nil
										   otherButtonTitles:@"1",@"2",@"3",@"4",@"5", nil];
	
	
	// Register for notifications
   [self registerForUIApplicationNotifications];
	
	_audioManager = [[BAGPAudioManager alloc] init];
	NSAssert(
	noErr == [_audioManager loadSynthFromPresetURL: [NSURL fileURLWithPath: [[NSBundle mainBundle] pathForResource: @"FluidR3 GM2-2" ofType: @".SF2"]]],
			 @"Error loading sound font");
	


}

// tap to zoom back to scale of 1
// TODO be nice if it stayed on the same spot...
- (void)handleTap:(UITapGestureRecognizer *)sender
{
	if (sender.state == UIGestureRecognizerStateEnded) {
		[self.fretboardScrollView setZoomScale: 1 animated:YES];
	}
}


- (void)handleToneTap:(UITapGestureRecognizer *)sender
{
	
	BAGPDisplayTone *sourceDisplayTone = (BAGPDisplayTone *) sender.view;
	if (sender.state == UIGestureRecognizerStateEnded) {
		NSLog(@"%s, %@, %ld, %ld",__PRETTY_FUNCTION__, sourceDisplayTone.text, (long) sourceDisplayTone.string, (long)sourceDisplayTone.fret);
 	    [_audioManager play];
	}

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	NSLog(@"scrollViewDidScroll: %f, %f",scrollView.contentOffset.x, scrollView.contentOffset.y);
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	NSLog(@"viewForZoomingInScrollView: ");

	id retVal = nil;
	
	if(scrollView == self.fretboardScrollView) {
		NSLog(@"viewForZoomingInScrollView fretboardZoom");

		retVal = self.fretboardView;
	}
	
	return retVal;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
	NSLog(@"scrollViewDidZoom zoomScale: %f",scrollView.zoomScale);
	//[self.fretboardView setZoomScale: scrollView.zoomScale];
	
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
	NSLog(@"scrollViewDidEndZooming zoomScale: %f",scrollView.zoomScale);
	[self.fretboardView setZoomScale: scrollView.zoomScale];
	
}

//- (void)centerScrollViewContents {
//	NSLog(@"centerScrollViewcontents");
//	
//    CGSize boundsSize = self.fretboardScrollView.bounds.size;
//    CGRect contentsFrame = self.fretboardView.frame;
//	
//    if (contentsFrame.size.width < boundsSize.width) {
//        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
//    } else {
//        contentsFrame.origin.x = 0.0f;
//    }
//	
//    if (contentsFrame.size.height < boundsSize.height) {
//        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
//    } else {
//        contentsFrame.origin.y = 0.0f;
//    }
//	
//    self.fretboardView.frame = contentsFrame;
//}

#pragma mark view update
- (void)updateDisplayTones
{

	// What tones to display
	NSArray *shapeInfo = [[BAGPPatternFactory instance] getShapeForKey: _keyBarButtonItem.title
															 scale: _scaleBarButtonItem.title
														   pattern: _patternBarButtonItem.title];
	NSArray *tones = shapeInfo[1];
	int shapeWidthInFrets = [shapeInfo[0] intValue];
	
	// Get lookup key, lookup which fret pattern starts on
	NSString *patternPositionKey = [NSString stringWithFormat: @"%@,%@,%d",
										_currentKey,
										_scaleBarButtonItem.title,
									_currentPattern];
	int fret = [_patternPositions[patternPositionKey] intValue];
	
	// Center the pattern in the rect (the ends take care of themselves)

	[_fretboardView centerOnFret: fret + (shapeWidthInFrets / 2)];
	

	// cleanup existing tones before creating more TODO: reuse?
	for (UIView* uiView in [_fretboardView subviews]) {
		[uiView removeFromSuperview];
	}
	
	for(NSArray *tone in tones) {
		
		CGRect toneRect = CGRectMake(kBAGPFretSpacing * ([tone[1] integerValue] + fret) - (kBAGPFretSpacing / 2),
									 kBAGPStringSpacing * [tone[0] integerValue] - (kBAGPStringSpacing / 2), 50, 50);
		BAGPDisplayTone *displayTone = [[BAGPDisplayTone alloc] initWithFrame: toneRect];
		
		displayTone.backgroundColor = [UIColor clearColor];
		
		displayTone.string = [tone[0] integerValue];
		displayTone.fret =  [tone[1] integerValue] + fret;
		
		displayTone.text = _tmpDict[_fretboardTones[displayTone.string][displayTone.fret]];
		displayTone.decoration = kToneDecorationNone;
		
		[displayTone addGestureRecognizer: [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(handleToneTap:)]];
		
		[displayTone setNeedsDisplay];
		
		[self.fretboardView addSubview: displayTone];
		
	}
	
	
	[self.fretboardView setNeedsDisplay];
}

#pragma mark UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSString *buttonTitle = [actionSheet buttonTitleAtIndex: buttonIndex];
	
	NSLog(@"%s, actionSheet clicked buttonAtIndex: %ld, %@",__PRETTY_FUNCTION__, (long)buttonIndex, buttonTitle );
	
	if (![buttonTitle isEqualToString: @"Cancel"] ) {
		
		if (actionSheet == _keyActionSheet) {
			_keyBarButtonItem.title = buttonTitle;
			_currentKey = buttonTitle;
		}
		
		if (actionSheet == _scaleActionSheet) {
			_scaleBarButtonItem.title = buttonTitle;
			// TODO keep track of current scale
		}
		
		if (actionSheet == _patternTypeActionsheet) {
			_patternTypeButtonItem.title = buttonTitle;
			// TODO keep track of current pattern type
			
		}
		
		if (actionSheet == _patternActionSheet) {
			_patternBarButtonItem.title =  [NSString stringWithFormat: @"Pattern: %@", buttonTitle ];
			_currentPattern = buttonIndex + 1;
		}
		
		[self updateDisplayTones];
		
	}

}

#pragma mark - toolbar button item handlers
- (IBAction)keyBarButtonPressed:(UIBarButtonItem *)sender {
	NSLog(@"%s",__PRETTY_FUNCTION__);
	
	[_keyActionSheet showFromBarButtonItem: sender animated:YES];
}

- (IBAction)scaleBarButtonPressed:(UIBarButtonItem *)sender {
	NSLog(@"%s",__PRETTY_FUNCTION__);
	[_scaleActionSheet showFromBarButtonItem: sender animated:YES];
}

- (IBAction)patternTypeBarButtonPressed:(UIBarButtonItem *)sender {
	NSLog(@"%s",__PRETTY_FUNCTION__);
	[_patternTypeActionsheet showFromBarButtonItem: sender animated: YES];
}

- (IBAction)patternBarButtonPressed:(UIBarButtonItem *)sender {
	NSLog(@"%s",__PRETTY_FUNCTION__);
	[_patternActionSheet showFromBarButtonItem: sender animated: YES];
}

#pragma mark - midi stuff
- (void) playMidiSound
{

//	MusicDeviceMIDIEvent (self.samplerUnit, noteCommand, noteNum, onVelocity, 0);

//    // Create a new music sequence
//    MusicSequence s;
//    // Initialise the music sequence
//    NewMusicSequence(&s);
//	
//	
//	
//    // Get a string to the path of the MIDI file which
//    // should be located in the Resources folder
//    // I'm using a simple test midi file which is included in the download bundle at the end of this document
//    //NSString *midiFilePath = [[NSBundle mainBundle]
//	//						  pathForResource:@"simpletest"
//	//						  ofType:@"mid"];
//	
//    // Create a new URL which points to the MIDI file
//    //NSURL * midiFileURL = [NSURL fileURLWithPath:midiFilePath];
//	
//	
//    //MusicSequenceFileLoad(s, CFBridgingRetain(midiFileURL), 0, 0);
//	MusicDeviceMIDIEvent(<#MusicDeviceComponent inUnit#>, <#UInt32 inStatus#>, <#UInt32 inData1#>, <#UInt32 inData2#>, <#UInt32 inOffsetSampleFrame#>)
//	
//    // Create a new music player
//    MusicPlayer  p;
//    // Initialise the music player
//    NewMusicPlayer(&p);
//	
//    // Load the sequence into the music player
//    MusicPlayerSetSequence(p, s);
//    // Called to do some MusicPlayer setup. This just
//    // reduces latency when MusicPlayerStart is called
//    MusicPlayerPreroll(p);
//    // Starts the music playing
//    MusicPlayerStart(p);
//	
//    // Get length of track so that we know how long to kill time for
//    MusicTrack t;
//    MusicTimeStamp len;
//    UInt32 sz = sizeof(MusicTimeStamp);
//    MusicSequenceGetIndTrack(s, 1, &t);
//    MusicTrackGetProperty(t, kSequenceTrackProperty_TrackLength, &len, &sz);
//	
//	
//    while (1) { // kill time until the music is over
//        usleep (3 * 1000 * 1000);
//        MusicTimeStamp now = 0;
//        MusicPlayerGetTime (p, &now);
//        if (now >= len)
//            break;
//    }
//	
//    // Stop the player and dispose of the objects
//    MusicPlayerStop(p);
//    DisposeMusicSequence(s);
//    DisposeMusicPlayer(p);
	
}

#pragma mark - Application state management

// The audio processing graph should not run when the screen is locked or when the app has
//  transitioned to the background, because there can be no user interaction in those states.
//  (Leaving the graph running with the screen locked wastes a significant amount of energy.)
//
// Responding to these UIApplication notifications allows this class to stop and restart the
//    graph as appropriate.
- (void) registerForUIApplicationNotifications {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver: self
                           selector: @selector (handleResigningActive:)
                               name: UIApplicationWillResignActiveNotification
                             object: [UIApplication sharedApplication]];
    
    [notificationCenter addObserver: self
                           selector: @selector (handleBecomingActive:)
                               name: UIApplicationDidBecomeActiveNotification
                             object: [UIApplication sharedApplication]];
}


- (void) handleResigningActive: (id) notification {

    // TODO stop any audio
//    [self stopPlayLowNote: self];
//    [self stopPlayMidNote: self];
//    [self stopPlayHighNote: self];
//    [self stopAudioProcessingGraph];
}


- (void) handleBecomingActive: (id) notification {
    // TODO restart graph
//    [self restartAudioProcessingGraph];
}

@end
