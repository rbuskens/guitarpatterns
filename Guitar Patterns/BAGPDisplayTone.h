//
//  BAGPDisplayTone.h
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-20.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//
//	A display object to support seperation of view from controller and the model.

#import <Foundation/Foundation.h>


enum kToneDecoration {
	kToneDecorationNone = 1,
	kToneDecorationRoot = 2,			// Root tone
	kToneDecorationPassingTone = 3,     // Common with tone in 'next' pattern
	kToneDecorationPrimaryTone = 4      // Establishes tonality in this pattern
	};

@interface BAGPDisplayTone : UIView
@property NSInteger string;
@property NSInteger fret;
@property (strong, nonatomic) NSString *text;
@property enum kToneDecoration decoration;

@end
