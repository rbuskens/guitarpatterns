//
//  BAGPDisplayTone.m
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-20.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import "BAGPDisplayTone.h"

@implementation BAGPDisplayTone
- (void)drawRect:(CGRect)rect
{
	
	// TODO put this in an initialiser
//	if(_zoomScale == 0) {
//		_zoomScale = 1.0;
//	}
	
    // Drawing code
	CGContextRef context = UIGraphicsGetCurrentContext();
		
	float x = self.bounds.size.width / 2;
	float y = self.bounds.size.height / 2;
		
	CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
		
	CGMutablePathRef path = CGPathCreateMutable();
		
	CGPathAddArc(path, NULL, x, y, self.bounds.size.width / 2 - 4, 0*3.142/180, 360*3.142/180, 0);
		
	CGContextAddPath(context, path);
		
	CGContextFillPath(context);
	CGPathRelease(path);
		
	CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
	[_text drawInRect: CGRectMake(x - 10, y - 15, 50, 10) withFont:[UIFont systemFontOfSize:24]];

}
@end
