//
//  BAGPViewController.h
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-08.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAGPFretboardView.h"
#import "BAGPConstants.h"

@interface BAGPViewController : UIViewController <UIScrollViewDelegate, UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet BAGPFretboardView *fretboardView;
@property (weak, nonatomic) IBOutlet UIScrollView *fretboardScrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *keyBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *scaleBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *patternTypeButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *patternBarButtonItem;


- (IBAction)keyBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)scaleBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)patternTypeBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)patternBarButtonPressed:(UIBarButtonItem *)sender;


@end
