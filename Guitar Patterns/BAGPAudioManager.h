//
//  BAGPAudioManager.h
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-27.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

@interface BAGPAudioManager : NSObject
- (OSStatus)    loadSynthFromPresetURL:(NSURL *) presetURL; // ??? public? return OSStatus?
- (void) startPlayNote: (UInt32) noteNbr;
- (void) stopPlayNote: (UInt32) noteNbr;
- (void) play;

@end
