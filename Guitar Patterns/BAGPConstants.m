//
//  BAGPConstants.m
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-31.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import "BAGPConstants.h"

int const kBAGPFretSpacing = 82;
int const kBAGPStringSpacing = 36;

@implementation BAGPConstants

@end
