//
//  BAGPPatternFactory.h
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-18.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BAGPPatternFactory : NSObject
+ (BAGPPatternFactory *) instance;

- (NSArray *) getShapeForKey: (NSString *) key scale: (NSString *) scale pattern: (NSString *) pattern;
@end
