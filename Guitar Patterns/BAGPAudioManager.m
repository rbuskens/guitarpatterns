//
//  BAGPAudioManager.m
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-27.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import "BAGPAudioManager.h"

// some MIDI constants:
enum {
	kMIDIMessage_NoteOn    = 0x9,
	kMIDIMessage_NoteOff   = 0x8,
};

#define kLowNote  48
#define kHighNote 72
#define kMidNote  60


@interface BAGPAudioManager()

@property (readwrite) Float64   graphSampleRate;
@property (readwrite) AUGraph   processingGraph;
@property (readwrite) AudioUnit samplerUnit;
@property (readwrite) AudioUnit ioUnit;

//- (OSStatus)    loadSynthFromPresetURL:(NSURL *) presetURL;
//- (void)        registerForUIApplicationNotifications;
- (BOOL)        createAUGraph;
- (void)        configureAndStartAudioProcessingGraph: (AUGraph) graph;
- (void)        stopAudioProcessingGraph;
- (void)        restartAudioProcessingGraph;
@end

@implementation BAGPAudioManager

//@synthesize graphSampleRate     = _graphSampleRate;
//@synthesize samplerUnit         = _samplerUnit;
//@synthesize ioUnit              = _ioUnit;
//@synthesize processingGraph     = _processingGraph;

#pragma mark -
#pragma mark Audio setup

- (id)init
{
    self = [super init];
	
    if (self) {
		// Set up the audio session for this app, in the process obtaining the
		// hardware sample rate for use in the audio processing graph.
		BOOL audioSessionActivated = [self setupAudioSession];
		NSAssert (audioSessionActivated == YES, @"Unable to set up audio session.");
		
		// Create the audio processing graph; place references to the graph and to the Sampler unit
		// into the processingGraph and samplerUnit instance variables.
		[self createAUGraph];
		[self configureAndStartAudioProcessingGraph: self.processingGraph];
    }

    return self;
}

// Create an audio processing graph.
- (BOOL) createAUGraph {
    
	OSStatus result = noErr;
	AUNode samplerNode, ioNode;
	
    // Specify the common portion of an audio unit's identify, used for both audio units
    // in the graph.
	AudioComponentDescription cd = {};
	cd.componentManufacturer     = kAudioUnitManufacturer_Apple;
	cd.componentFlags            = 0;
	cd.componentFlagsMask        = 0;
	
    // Instantiate an audio processing graph
	result = NewAUGraph (&_processingGraph);
    NSCAssert (result == noErr, @"Unable to create an AUGraph object. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
	//Specify the Sampler unit, to be used as the first node of the graph
	cd.componentType = kAudioUnitType_MusicDevice;
	cd.componentSubType = kAudioUnitSubType_Sampler;
	
    // Add the Sampler unit node to the graph
	result = AUGraphAddNode (self.processingGraph, &cd, &samplerNode);
    NSCAssert (result == noErr, @"Unable to add the Sampler unit to the audio processing graph. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
	// Specify the Output unit, to be used as the second and final node of the graph
	cd.componentType = kAudioUnitType_Output;
	cd.componentSubType = kAudioUnitSubType_RemoteIO;
	
    // Add the Output unit node to the graph
	result = AUGraphAddNode (self.processingGraph, &cd, &ioNode);
    NSCAssert (result == noErr, @"Unable to add the Output unit to the audio processing graph. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
    // Open the graph
	result = AUGraphOpen (self.processingGraph);
    NSCAssert (result == noErr, @"Unable to open the audio processing graph. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
    // Connect the Sampler unit to the output unit
	result = AUGraphConnectNodeInput (self.processingGraph, samplerNode, 0, ioNode, 0);
    NSCAssert (result == noErr, @"Unable to interconnect the nodes in the audio processing graph. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
	// Obtain a reference to the Sampler unit from its node
	result = AUGraphNodeInfo (self.processingGraph, samplerNode, 0, &_samplerUnit);
    NSCAssert (result == noErr, @"Unable to obtain a reference to the Sampler unit. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
	// Obtain a reference to the I/O unit from its node
	result = AUGraphNodeInfo (self.processingGraph, ioNode, 0, &_ioUnit);
    NSCAssert (result == noErr, @"Unable to obtain a reference to the I/O unit. Error code: %d '%.4s'", (int) result, (const char *)&result);
    
    return YES;
}


// Starting with instantiated audio processing graph, configure its
// audio units, initialize it, and start it.
- (void) configureAndStartAudioProcessingGraph: (AUGraph) graph {
	
    OSStatus result = noErr;
    UInt32 framesPerSlice = 0;
    UInt32 framesPerSlicePropertySize = sizeof (framesPerSlice);
    UInt32 sampleRatePropertySize = sizeof (self.graphSampleRate);
    
    result = AudioUnitInitialize (self.ioUnit);
    NSCAssert (result == noErr, @"Unable to initialize the I/O unit. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
    // Set the I/O unit's output sample rate.
    result =    AudioUnitSetProperty (
									  self.ioUnit,
									  kAudioUnitProperty_SampleRate,
									  kAudioUnitScope_Output,
									  0,
									  &_graphSampleRate,
									  sampleRatePropertySize
									  );
    
    NSAssert (result == noErr, @"AudioUnitSetProperty (set Sampler unit output stream sample rate). Error code: %d '%.4s'", (int) result, (const char *)&result);
	
    // Obtain the value of the maximum-frames-per-slice from the I/O unit.
    result =    AudioUnitGetProperty (
									  self.ioUnit,
									  kAudioUnitProperty_MaximumFramesPerSlice,
									  kAudioUnitScope_Global,
									  0,
									  &framesPerSlice,
									  &framesPerSlicePropertySize
									  );
	
    NSCAssert (result == noErr, @"Unable to retrieve the maximum frames per slice property from the I/O unit. Error code: %d '%.4s'", (int) result, (const char *)&result);
	
    // Set the Sampler unit's output sample rate.
    result =    AudioUnitSetProperty (
									  self.samplerUnit,
									  kAudioUnitProperty_SampleRate,
									  kAudioUnitScope_Output,
									  0,
									  &_graphSampleRate,
									  sampleRatePropertySize
									  );
    
    NSAssert (result == noErr, @"AudioUnitSetProperty (set Sampler unit output stream sample rate). Error code: %d '%.4s'", (int) result, (const char *)&result);
	
    // Set the Sampler unit's maximum frames-per-slice.
    result =    AudioUnitSetProperty (
									  self.samplerUnit,
									  kAudioUnitProperty_MaximumFramesPerSlice,
									  kAudioUnitScope_Global,
									  0,
									  &framesPerSlice,
									  framesPerSlicePropertySize
									  );
    
    NSAssert( result == noErr, @"AudioUnitSetProperty (set Sampler unit maximum frames per slice). Error code: %d '%.4s'", (int) result, (const char *)&result);
    
    
    if (graph) {
        
        // Initialize the audio processing graph.
        result = AUGraphInitialize (graph);
        NSAssert (result == noErr, @"Unable to initialze AUGraph object. Error code: %d '%.4s'", (int) result, (const char *)&result);
        
        // Start the graph
        result = AUGraphStart (graph);
        NSAssert (result == noErr, @"Unable to start audio processing graph. Error code: %d '%.4s'", (int) result, (const char *)&result);
        
        // Print out the graph to the console
        CAShow (graph);
    }
}

// Load a synthesizer preset file and apply it to the Sampler unit
- (OSStatus) loadSynthFromPresetURL: (NSURL *) presetURL {
	OSStatus result;
	
	AUSamplerBankPresetData bpdata;
	bpdata.bankURL  = (__bridge CFURLRef) presetURL;
	
	bpdata.bankMSB  = kAUSampler_DefaultMelodicBankMSB;
	bpdata.bankLSB  = kAUSampler_DefaultBankLSB;
	
	bpdata.presetID = (UInt8) 25;
		
	// set the kAUSamplerProperty_LoadPresetFromBank property
	result = AudioUnitSetProperty(self.samplerUnit,
								  kAUSamplerProperty_LoadPresetFromBank,
								  kAudioUnitScope_Global,
								  0,
								  &bpdata,
								  sizeof(bpdata));
		
		
	// check for errors
	NSCAssert (result == noErr,
	   @"Unable to set the preset property on the Sampler. Error code:%d '%.4s'",
	   (int) result,
	   (const char *)&result);
	
	
	return result;
}


// Set up the audio session for this app.
- (BOOL) setupAudioSession {
    
    AVAudioSession *mySession = [AVAudioSession sharedInstance];
    
    // Specify that this object is the delegate of the audio session, so that
    //    this object's endInterruption method will be invoked when needed.
    //[mySession setDelegate: self]; - deprecated
    
    // Assign the Playback category to the audio session. This category supports
    //    audio output with the Ring/Silent switch in the Silent position.
    NSError *audioSessionError = nil;
    [mySession setCategory: AVAudioSessionCategoryPlayback error: &audioSessionError];
    if (audioSessionError != nil) {NSLog (@"Error setting audio session category."); return NO;}
	
    // Request a desired hardware sample rate.
    self.graphSampleRate = 44100.0;    // Hertz
    
/* deprecated - TODO - documentation has alternatives
    [mySession setPreferredHardwareSampleRate: self.graphSampleRate error: &audioSessionError];
    if (audioSessionError != nil) {NSLog (@"Error setting preferred hardware sample rate."); return NO;}
*/
    // Activate the audio session
    [mySession setActive: YES error: &audioSessionError];
    if (audioSessionError != nil) {NSLog (@"Error activating the audio session."); return NO;}

/* deprecated
    // Obtain the actual hardware sample rate and store it for later use in the audio processing graph.
    self.graphSampleRate = [mySession currentHardwareSampleRate];
*/
	
    return YES;
}


#pragma mark -
#pragma mark Audio control
// Play note
- (void) startPlayNote: (UInt32) noteNbr {
	
	UInt32 onVelocity = 127;
	UInt32 noteCommand = 	kMIDIMessage_NoteOn << 4 | 0;
    
    OSStatus result = noErr;
	NSAssert(MusicDeviceMIDIEvent (self.samplerUnit, noteCommand, noteNbr, onVelocity, 0) == noErr, @"%s> error playing note with error code: %d",__PRETTY_FUNCTION__, (int)result);
}

// Stop the low note
- (void) stopPlayNote:(UInt32) noteNbr {
	
	UInt32 noteCommand = 	kMIDIMessage_NoteOff << 4 | 0;
	
    OSStatus result = noErr;
	NSAssert(MusicDeviceMIDIEvent (self.samplerUnit, noteCommand, noteNbr, 0, 0) == noErr, @"%s> error playing note with error code: %d",__PRETTY_FUNCTION__, (int)result);
}

- (void) play
{
	MusicPlayer musicPlayer;
	MusicSequence musicSequence;
	OSStatus osStatus;
	MIDIClientRef client;

	// Create a client
	osStatus = MIDIClientCreate(CFSTR("GuitarMidiClient"), NULL, NULL, &client);
	NSAssert (osStatus == noErr, @"Unable to create midi client. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);
	
	// Create an endpoint
//	MIDIEndpointRef endpoint;
//	osStatus = MIDIDestinationCreate(client, (CFStringRef) @"GuitarMidiTest", NULL, NULL, &endpoint);
//	NSAssert (osStatus == noErr, @"Unable to create midi destinayion. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);
	
	osStatus = NewMusicPlayer(&musicPlayer);
	NSAssert (osStatus == noErr, @"Unable to create music player. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);

	osStatus = NewMusicSequence(&musicSequence);
	NSAssert (osStatus == noErr, @"Unable to create music sequence. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);

	MusicTrack track;
	osStatus = MusicSequenceNewTrack(musicSequence,  &track);
	NSAssert (osStatus == noErr, @"Unable to create new track. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);

    MIDINoteMessage note;
    note.channel = (UInt8) 0;
    note.note = (UInt8) 58;
    note.velocity = (UInt8) 100;
    note.duration = (Float32) 4.0f;
    note.releaseVelocity = (UInt8) 0;
	
    MusicTrackNewMIDINoteEvent(track,  (MusicTimeStamp) (5),  &note);
	
	osStatus = MusicSequenceSetAUGraph(musicSequence, _processingGraph);
	NSAssert (osStatus == noErr, @"Unable to set augraph. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);
	
//	osStatus = MusicSequenceSetMIDIEndpoint(musicSequence, endpoint);
//	NSAssert (osStatus == noErr, @"Unable to set midi endpoint. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);
	
	osStatus = MusicPlayerSetSequence(musicPlayer, musicSequence);
	NSAssert (osStatus == noErr, @"Unable to set player sequence. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);
	
	osStatus = MusicPlayerPreroll(musicPlayer);
	NSAssert (osStatus == noErr, @"Unable to preroll player. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);
	
	osStatus = MusicPlayerStart(musicPlayer);
	NSAssert (osStatus == noErr, @"Unable to start player. Error code: %d '%.4s'", (int) osStatus, (const char *)&osStatus);
	
	
}

// Stop the audio processing graph
- (void) stopAudioProcessingGraph {
	
    OSStatus result = noErr;
	if (self.processingGraph) result = AUGraphStop(self.processingGraph);
    NSAssert (result == noErr, @"Unable to stop the audio processing graph. Error code: %d '%.4s'", (int) result, (const char *)&result);
}

// Restart the audio processing graph
- (void) restartAudioProcessingGraph {
	
    OSStatus result = noErr;
	if (self.processingGraph) result = AUGraphStart (self.processingGraph);
    NSAssert (result == noErr, @"Unable to restart the audio processing graph. Error code: %d '%.4s'", (int) result, (const char *)&result);
}


@end
