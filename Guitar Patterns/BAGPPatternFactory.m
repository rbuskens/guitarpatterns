//
//  BAGPPatternFactory.m
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-18.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import "BAGPPatternFactory.h"

static BAGPPatternFactory *sharedSingleton;

@interface BAGPPatternFactory()
@property (strong, nonatomic) NSDictionary *majorAndModeForms;
@end


@implementation BAGPPatternFactory


+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedSingleton = [[BAGPPatternFactory alloc] init];
		
		// For a readable form (relative) see the GuitarPatterns2.xls
		// "key" : width in frets, tones
		sharedSingleton.majorAndModeForms = @{
											  @"Pattern: 1" : @[
													  @4, @[
														  @[@1,@0],	@[@1,@1],		@[@1,@3],
														  @[@2,@0],	@[@2,@1],		@[@2,@3],
														  @[@3,@0],		@[@3,@2],
														  @[@4,@0],		@[@4,@2],	@[@4,@3],
														  @[@5,@0],		@[@5,@2],	@[@5,@3],
														  @[@6,@0],	@[@6,@1],		@[@6,@3]
														  ]
													  ],
											  
											  @"Pattern: 2" : @[
													  @6, @[
														  @[@1,@2],		@[@1,@4],		@[@1,@6],
														  @[@2,@2],		@[@2,@4],	@[@2,@5],
														  @[@3,@1],		@[@3,@3],	@[@3,@4],
														  @[@4,@1],	@[@4,@2],		@[@4,@4],
														  @[@5,@1],	@[@5,@2],		@[@5,@4],
														  @[@6,@0],		@[@6,@2],		@[@6,@4]
														  ]
													  ],
											  
											  @"Pattern: 3" : @[
													  @5, @[
														  @[@1,@1],		@[@1,@3],	@[@1,@4],
														  @[@2,@1],	@[@2,@2],		@[@2,@4],
														  @[@3,@0],	@[@3,@1],		@[@3,@3],
														  @[@4,@1],		@[@4,@3],
														  @[@5,@1],		@[@5,@3],	@[@5,@4],
														  @[@6,@1],		@[@6,@3],	@[@6,@4]
														  ]
													  ],
											  
											  @"Pattern: 4" : @[
													  @4, @[
														  @[@1,@0],	@[@1,@1],		@[@1,@3],
														  @[@2,@1],		@[@2,@3],
														  @[@3,@0],		@[@3,@2],	@[@3,@3],
														  @[@4,@0],		@[@4,@2],	@[@4,@3],
														  @[@5,@0],	@[@5,@1],		@[@5,@3],
														  @[@6,@0],	@[@6,@1],		@[@6,@3]
														  ]
													  ],
											  
											  @"Pattern: 5" : @[
													  @5, @[
														  @[@1,@1],		@[@1,@3],	@[@1,@4],
														  @[@2,@1],		@[@2,@3],	@[@2,@4],
														  @[@3,@0],	@[@3,@1],		@[@3,@3],
														  @[@4,@0],	@[@4,@1],		@[@4,@3],
														  @[@5,@1],		@[@5,@3],
														  @[@6,@1],		@[@6,@3],	@[@6,@4]
														  ]
													  ],
											  
											  };

    }
}

+ (BAGPPatternFactory *) instance
{
	return sharedSingleton;
}


- (NSArray *) getShapeForKey: (NSString *) key scale: (NSString *) scale pattern: (NSString *) pattern;
{
	
	// Starting point is the 5 patterns for the modes of the major scale
	//
	//
	
	
	
	return _majorAndModeForms[pattern];
}
@end
