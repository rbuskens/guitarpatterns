//
//  BAGPFretboardView.m
//  Guitar Patterns
//
//  Created by Rob Buskens on 2013-07-10.
//  Copyright (c) 2013 Buskens & Associates Inc. All rights reserved.
//

#import "BAGPConstants.h"
#import "BAGPFretboardView.h"
#import "BAGPDisplayTone.h"

@interface BAGPFretboardView()

@end

@implementation BAGPFretboardView

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//		
//	}
//	
//    return self;
//}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
	
	// TODO put this in an initialiser
	if(_zoomScale == 0) {
		_zoomScale = 1.0;
	}
	
    // Drawing code
	CGContextRef context = UIGraphicsGetCurrentContext();

	// Fretboard
	UIColor *mahogany = [UIColor colorWithRed: 128.0 / 256 green: 24.0 / 256 blue: 44 / 256 alpha: 1.0];
	CGContextSetFillColorWithColor(context, mahogany.CGColor);
	CGContextFillRect(context,
			 CGRectMake (10 * _zoomScale,
						 (kBAGPStringSpacing - 4) * _zoomScale,
						 (self.bounds.size.width - 20) * _zoomScale,
						 ((5 * kBAGPStringSpacing) + 4 ) * _zoomScale));

	// NUT
	UIColor *ivory = [UIColor colorWithRed: 255.0 / 256 green: 255.0 / 256 blue: 240 / 256 alpha: 1.0];
	CGContextSetFillColorWithColor(context, ivory.CGColor);
	CGContextFillRect(context, CGRectMake (10 * _zoomScale, (kBAGPStringSpacing - 4) * _zoomScale,
										   15 * _zoomScale, ((5 * kBAGPStringSpacing) + 4 ) * _zoomScale));

	// Frets
	CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed: 172.0 / 256 green: 172.0 / 256 blue: 172.0 / 256 alpha:1.0].CGColor);
	CGContextSetLineWidth(context, 5.0);
	
	for (int fret = 1; fret <= 24; fret++) {
		CGContextMoveToPoint(context, (10.0 + (fret * kBAGPFretSpacing)) * _zoomScale, (kBAGPStringSpacing - 4.0) * _zoomScale);
		CGContextAddLineToPoint(context, (10.0 + (fret * kBAGPFretSpacing)) * _zoomScale, ((kBAGPStringSpacing * 6) + 4) * _zoomScale);
		
	}

	CGContextStrokePath(context);
	
	// Strings
	CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed: 195.0 / 256 green: 195.0 / 256 blue: 89.0 / 256 alpha:1.0].CGColor);
	CGContextSetLineWidth(context, 5.0);

	for (int string = 1; string <= 6; string++) {
		CGContextMoveToPoint(context, 10.0 * _zoomScale, kBAGPStringSpacing * string * _zoomScale);
		CGContextAddLineToPoint(context, (self.bounds.size.width - 10) * _zoomScale, kBAGPStringSpacing * string * _zoomScale);

		
	}
	CGContextStrokePath(context);
}

- (void) centerOnFret: (int) fretNumber
{
	UIScrollView *parent = (UIScrollView *) [self superview]; // ??? seems a bit heinous assuming what your parent is.
	
	CGRect displayRect = CGRectMake(kBAGPFretSpacing * fretNumber, 0, kBAGPFretSpacing *7, parent.bounds.size.height);
	
	[parent scrollRectToVisible: displayRect animated: YES];
}
@end
